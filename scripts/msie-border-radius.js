function makeBorderRadius() {
  var elements = document.all;
  for ( var e,i=0; e=elements[i]; i++ ) {
    var radius = e.currentStyle['border-radius'];
    if (radius && radius != '0') {
      try { jQuery(e).corner(radius) }
      catch(err) {/* can't applay jQuery corner in some elements, like inputs */}
    }
  }
}

if ( navigator.appVersion.indexOf("MSIE") > -1 ) {
  var MSIE_VERSION = parseInt( navigator.appVersion.replace(/^.*MSIE\s+([0-9]+).*$/, "$1") );
  if ( MSIE_VERSION < 9 ) {
    window.attachEvent("onload", makeBorderRadius);
  }
}
